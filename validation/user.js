const { check } = require('express-validator');

exports.userSignupValidator = [
    check('name')
        .not()
        .isEmpty()
        .withMessage('fullname is required'),
    check('email')
        .isEmail()
        .withMessage('Enter valid email address'),
    check('password')
        .isLength({ min: 1 })
        .withMessage('Password Required'),
    check('confirm_password')
        .isLength({ min: 1 })
        .withMessage('Confirm Password Required'),
    check('password').custom((value, { req }) => {
        if (value !== req.body.confirm_password) {
            throw new Error("Password not match.");
        } else {
            return true;
        }
    })
];

exports.userSigninValidator = [
    check('email')
        .isEmail()
        .withMessage('Enter valid email address'),
    check('password')
        .isLength({ min: 1 })
        .withMessage('Password Required')
];

exports.userForgotValidator = [
    check('email')
        .isEmail()
        .withMessage('Enter valid email address')
];

exports.userResetValidator = [
    check('token')
        .not()
        .isEmpty()
        .withMessage('Enter valid token'),
    check('password')
        .isLength({ min: 1 })
        .withMessage('Password Required'),
    check('confirm_password')
        .isLength({ min: 1 })
        .withMessage('Confirm Password Required'),
    // check('password').custom((value, { req }) => {
    //     if (value !== req.body.confirm_password) {
    //         throw new Error("Password not match.");
    //     } else {
    //         return true;
    //     }
    // })
];