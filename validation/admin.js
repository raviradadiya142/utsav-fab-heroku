const { check } = require('express-validator');

exports.signinValidator = [
    check('username')
        .isLength({ min: 1 })
        .withMessage('username required'),
    check('password')
        .isLength({ min: 1 })
        .withMessage('Password Required.')
];