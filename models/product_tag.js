const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    tag_name: { type: String },
}, { versionKey: false, timestamps: true });

const ProductTag = mongoose.model('product_tag', modelSchema, 'product_tag');
module.exports = ProductTag;