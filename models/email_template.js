//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    title: { type: String },
    subject: { type: String },
    from: { type: String },
    reply: { type: String },
    long_dec: { type: String },
    is_active: { type: Boolean, default: false },
    image: { type: String },
}, { versionKey: false, timestamps: true });

const EmailTemplate = mongoose.model('email_template', modelSchema, 'email_template');
module.exports = EmailTemplate;