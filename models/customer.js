const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    email: { type: String, lowercase: true },
    password: { type: String },
    fist_name: { type: String },
    last_name: { type: String },
    company_name: { type: String },
    gender: { type: String },
    address_type: { type: String },
    customer_role: { type: String },
    is_active: { type: Boolean, default: false },
    news_letter: { type: Boolean, default: false },
    session: { types: Number },
    country_id: { type: mongoose.Schema.Types.ObjectId, ref: 'country' },
    ip_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ip' }],
    order_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'order' }],
    address_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'address' }],
    cart_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'cart' }],
    wish_list_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'wish_list' }],
    activity_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'activity' }],
}, { versionKey: false, timestamps: true });

const Customer = mongoose.model('customer', modelSchema, 'customer');
module.exports = Customer;