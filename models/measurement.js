//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    short_dec: { type: String },
    available: { type: Boolean, default: false },
    image: { type: String },
    measurement_value_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "measurement_value" }]
}, { versionKey: false, timestamps: true });

const Measurement = mongoose.model('measurement', modelSchema, 'measurement');
module.exports = Measurement;