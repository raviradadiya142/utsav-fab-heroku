//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    title: { type: String },
    short_dec: { type: String },
    long_dec: { type: String },
    page_url: { type: String },
    display_order: { type: Number },
    search_engine_page_name: { type: String },
    meta_title: { type: String },
    meta_keyword: { type: Array },
    meta_dec: { type: String },
    meta_tag: { type: Array },
    images: { type: String },
}, { versionKey: false, timestamps: true });

const Page = mongoose.model('page', modelSchema, 'page');
module.exports = Page;