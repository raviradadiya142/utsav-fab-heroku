const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    display_order: { type: Number },
    state_id: { type: mongoose.Schema.Types.ObjectId, ref: 'state' }
}, { versionKey: false, timestamps: true });

const City = mongoose.model('city', modelSchema, 'city');
module.exports = City;