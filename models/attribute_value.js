const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    display_order: { type: Number },
    short_dec: { type: String },
    long_dec: { type: String },
    meta_title: { type: String },
    meta_dec: { type: String },
    meta_keyword: { type: Array },
    meta_tag: { type: Array },
    price: { type: Number },
    quantity: { type: Number },
    ship_day: { type: Number },
    published: { type: Boolean },
    image: { type: String },
    product_attribute_id: { type: mongoose.Schema.Types.ObjectId, ref:"product_attribute"},
}, { versionKey: false, timestamps: true });

const AttributeValue = mongoose.model('attribute_value', modelSchema, 'attribute_value');
module.exports = AttributeValue;