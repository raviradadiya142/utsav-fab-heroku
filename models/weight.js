const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    weight: { type: Number },
    rate: { type: Number },
    country_id: { type: mongoose.Schema.Types.ObjectId, ref: 'country' }
}, { versionKey: false, timestamps: true });

const Weight = mongoose.model('weight', modelSchema, 'weight');
module.exports = Weight;