//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    parent_category_id: { type: mongoose.Schema.Types.ObjectId, ref: "parent_category" },
    display_order: { type: Number },
    short_dec: { type: String },
    long_dec: { type: String },
    meta_title: { type: String },
    meta_dec: { type: String },
    // meta_keyword: { type: String },
    meta_keyword: { type: Array },
    // meta_tag: { type: String },
    meta_tag: { type: Array },
    published: { type: Boolean, default: false },
    image: { type: String },
}, { versionKey: false, timestamps: true });

const BlogCategory = mongoose.model('blog_category', modelSchema, 'blog_category');
module.exports = BlogCategory;