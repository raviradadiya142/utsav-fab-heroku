const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    initial_value: { type: String },
    initial_percentage: { type: Number },
    coupon_code: { type: String },
    customer_name: { type: String },
    sender_name: { type: String },
    sender_email: { type: String },
    message: { type: String },
    gift_card_active: { type: Boolean },
}, { versionKey: false, timestamps: true });

const ProductGiftCard = mongoose.model('product_gift_card', modelSchema, 'product_gift_card');
module.exports = ProductGiftCard;