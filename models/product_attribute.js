const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    display_order: { type: Number },
    short_dec: { type: String },
    long_dec: { type: String },
    meta_title: { type: String },
    meta_dec: { type: String },
    meta_keyword: { type: Array },
    meta_tag: { type: Array },
    published: { type: Boolean },
    image: { type: String },
    attribute_value_id: { type: mongoose.Schema.Types.ObjectId, ref:"attribute_value"},
}, { versionKey: false, timestamps: true });

const ProductAttribute = mongoose.model('product_attribute', modelSchema, 'product_attribute');
module.exports = ProductAttribute;