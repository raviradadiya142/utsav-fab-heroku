//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    title: { type: String },
    url: { type: String },
    author: { type: String },
    short_dec: { type: String },
    long_dec: { type: String },
    blog_category_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "blog_category" }],
    search_engine_page_name: { type: String },
    meta_title: { type: String },
    meta_dec: { type: String },
    meta_keyword: { type: Array },
    meta_tag: { type: Array },
    allow_comment: { type: String },
    published: { type: Boolean, default: false },
    image: { type: String },
}, { versionKey: false, timestamps: true });

const BlogPost = mongoose.model('blog_post', modelSchema, 'blog_post');
module.exports = BlogPost;