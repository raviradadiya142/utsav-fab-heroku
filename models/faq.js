//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    question: { type: String },
    answer: { type: String },
    published: { type: Boolean, default: false },
    help_id: { type: mongoose.Schema.Types.ObjectId, ref: "help" }
}, { versionKey: false, timestamps: true });

const FAQ = mongoose.model('faq', modelSchema, 'faq');
module.exports = FAQ;