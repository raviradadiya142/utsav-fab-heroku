//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    short_dec: { type: String },
    long_dec: { type: String },
    image: { type: String },
}, { versionKey: false, timestamps: true });

const HowToMeasure = mongoose.model('how_to_measure', modelSchema, 'how_to_measure');
module.exports = HowToMeasure;