const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    attributes_name: { type: String },
    attributes_url: { type: String },
    product_attribute_id: { type: mongoose.Schema.Types.ObjectId, ref:"product_attribute" },
    short_description: { type: String },
    long_description: { type: String },
    meta_title: { type: String },
    meta_keyword: { type: String },
    meta_description: { type: String },
    meta_tag: { type: String },
}, { versionKey: false, timestamps: true });

const ProductCombination = mongoose.model('product_combination', modelSchema, 'product_combination');
module.exports = ProductCombination;