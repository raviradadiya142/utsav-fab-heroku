const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    description: { type: String },
    meta_title: { type: String },
    meta_keyword: { type: String },
    meta_description: { type: String },
    meta_tag: { type: String },
    published: { type: Boolean },
    featured: { type: Boolean },
    category_image: { type: String }
}, { versionKey: false, timestamps: true });

const ParentCategory = mongoose.model('parent_category', modelSchema, 'parent_category');
module.exports = ParentCategory;