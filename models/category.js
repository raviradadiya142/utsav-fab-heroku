const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    parent_id: { type: mongoose.Schema.Types.ObjectId, ref: "parent_category" },
    display_order: { type: String },
    short_dec: { type: String },
    long_dec: { type: String },
    meta_title: { type: String },
    meta_keyword: { type: String },
    meta_dec: { type: String },
    meta_tag: { type: String },
    published: { type: Boolean, default: false },
    featured: { type: Boolean, default: false },
    featured_image: { type: String },
    category_image: { type: String }
}, { versionKey: false, timestamps: true });

const Category = mongoose.model('category', modelSchema, 'category');
module.exports = Category;