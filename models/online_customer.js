const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    customer_id: { type: mongoose.Schema.Types.ObjectId, ref: 'customer' }
}, { versionKey: false, timestamps: true });

const OnlineCustomer = mongoose.model('online_customer', modelSchema, 'online_customer');
module.exports = OnlineCustomer;