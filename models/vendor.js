const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    email: { type: String },
    slug: { type: String },
    description: { type: String },
    meta_title: { type: String },
    meta_keyword: { type: String },
    meta_tag: { type: String },
    meta_description: { type: String },
    first_name: { type: String },
    last_name: { type: String },
    company: { type: String },
    country_id: { type: mongoose.Schema.Types.ObjectId, ref: "country" },
    state_id: { type: mongoose.Schema.Types.ObjectId, ref: "state" },
    city_id: { type: mongoose.Schema.Types.ObjectId, ref: "city" },
    zip_number: { type: Number },
    phone_number: { type: Object },
    alternate_phone_number: { type: Object },
    whatsapp_number: { type: Object },
    bank_name: { type: String },
    bank_account_name: { type: String },
    bank_account_number: { type: String },
    bank_ifsc_code: { type: String },
    gst_number: { type: String },
    bank_pan_number: { type: String },
    vendor_note: { type: String },
    active: { type: Boolean },
    image: { type: String }
}, { versionKey: false, timestamps: true });

const Vendor = mongoose.model('vendor', modelSchema, 'vendor');
module.exports = Vendor;