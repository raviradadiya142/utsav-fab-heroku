const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    short_description: { type: String },
    long_description: { type: String },
    parent_category_id: { type: mongoose.Schema.Types.ObjectId, ref: "parent_category" },
    meta_title: { type: String },
    meta_description: { type: String },
    meta_keyword: { type: String },
    meta_tag: { type: String },
    product_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "product" }],
    published: { type: Boolean },
    image: { type: String },
}, { versionKey: false, timestamps: true });

const ManualPage = mongoose.model('manual_page', modelSchema, 'manual_page');
module.exports = ManualPage;