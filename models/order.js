const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    order_status: { type: String },
    order_number: { type: String },
    order_date: { type: String },
    customer_email: { type: String },
    customer_ip: { type: String },
    address_type: { type: String },
    shipping_method: { type: String },
    payment_status: { type: String },
    payment_method: { type: String },
    transaction_id: { type: String },
    order_subtotal: { type: Number },
    order_shipping: { type: Number },
    gift_card: { type: Number },
    order_total: { type: Number },
    order_note: { type: String },
    product_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "product" }],
}, { versionKey: false, timestamps: true });

const Order = mongoose.model('order', modelSchema, 'order');
module.exports = Order;