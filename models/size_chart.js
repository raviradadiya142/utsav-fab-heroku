//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    slug: { type: String },
    short_dec: { type: String },
    long_dec: { type: String },
    image: { type: String },
}, { versionKey: false, timestamps: true });

const SizeChart = mongoose.model('size_chart', modelSchema, 'size_chart');
module.exports = SizeChart;