const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    name: { type: String },
    abbreviation: { type: String },
    display_order: { type: Number },
    published: { type: Boolean, default: false },
    city_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'city' }],
    country_id: { type: mongoose.Schema.Types.ObjectId, ref: 'country' }
}, { versionKey: false, timestamps: true });

const State = mongoose.model('state', modelSchema, 'state');
module.exports = State;