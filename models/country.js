//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    two_digit_code: { type: String },
    three_digit_code: { type: String },
    currency_code: { type: String },
    currency_symbol: { type: String },
    mobile_extension: { type: String },
    language: { type: String },
    inr_to_rate: { type: Number },
    percentage: { type: Number },
    display_order: { type: Number, default: 0 },
    allow_shipping: { type: Boolean, default: false },
    allow_billing: { type: Boolean, default: false },
    subject_to_vat: { type: Boolean, default: false },
    published: { type: Boolean, default: false },
    state_id: [{ type: mongoose.Schema.Types.ObjectId, ref: 'state' }],
    weight: [{ type: mongoose.Schema.Types.ObjectId, ref: 'weight' }]
}, { versionKey: false, timestamps: true });

const Country = mongoose.model('country', modelSchema, 'country');
module.exports = Country;