//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    title: { type: String },
    attribute_id: { type: mongoose.Schema.Types.ObjectId, ref: "product_attribute" },
    short_dec: { type: String },
    from: { type: Number },
    to: { type: Number },
    img_with_text: { type: Array },
    radio: { type: String },
    measurement_id: { type: mongoose.Schema.Types.ObjectId, ref: "measurement" }
}, { versionKey: false, timestamps: true });

const MeasurementValue = mongoose.model('measurement_value', modelSchema, 'measurement_value');
module.exports = MeasurementValue;