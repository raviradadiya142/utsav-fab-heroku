const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    username: { type: String },
    password: { type: String },
    security_code: { type: String },
    keep_login: { type: Boolean },
    flag: { type: Number },
}, { versionKey: false, timestamps: true });

const Admin = mongoose.model('admin', modelSchema, 'admin');
module.exports = Admin;