//Require Mongoose
const mongoose = require('mongoose');

//Define a schema
const modelSchema = new mongoose.Schema({
    name: { type: String },
    display_order: { type: Number },
    short_dec: { type: String },
    long_dec: { type: String },
    meta_keyword: { type: String },
    meta_title: { type: String },
    meta_dec: { type: String },
    meta_tag: { type: Array },
    published: { type: Boolean, default: false },
    image: { type: String },
    category_id: { type: mongoose.Schema.Types.ObjectId, ref: "parent_category" },
    faq_id: [{ type: mongoose.Schema.Types.ObjectId, ref: "faq" }],
}, { versionKey: false, timestamps: true });

const Help = mongoose.model('help', modelSchema, 'help');
module.exports = Help;