const { commonQuery, common_error_message } = require('../../helper/common_helper');

// import modules
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// import models file
const Customer = require('../../models/customer');
const Country = require('../../models/country');
const State = require('../../models/state');
const Weight = require('../../models/weight');
const City = require('../../models/city');
const Page = require('../../models/page');
const SizeChart = require('../../models/size_chart');
const Measurement = require('../../models/measurement');
const MeasurementValue = require('../../models/measurement_value.js');
const HowtoMeasure = require('../../models/how_to_measure');
const BlogCategory = require('../../models/blog_category');
const BlogPost = require('../../models/blog_post');
const EmailTemplate = require('../../models/email_template');
const Help = require('../../models/help');
const FAQ = require('../../models/faq');

//Ravi Implement
const StoreDetails = require('../../models/store_deatils')
const Vendor = require('../../models/vendor')
const ParentCategory = require('../../models/parent_category')
const Category = require('../../models/category')
const ProductTag = require('../../models/product_tag')
const ProductAttribute = require('../../models/product_attribute')
const AttributeValue = require('../../models/attribute_value')
const ProductCombination = require('../../models/product_attributes_combination')
const Product = require('../../models/product')
const ManualPage = require('../../models/manual_page')
const ProductGiftCard = require('../../models/product_gift_card')
const Order = require('../../models/order')


module.exports = {
    allCustomers: async (req, res) => {
        let users_data = await commonQuery(Customer, "find", {});
        if (users_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = users_data.data;
            res.status(200).json(data)
        }
    },

    allCountries: async (req, res) => {
        let datas_data = await commonQuery(Country, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneCountry: async (req, res) => {
        const get_data = await commonQuery(Country, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createCountry: async (req, res) => {
        const { name, two_digit_code, three_digit_code, currency_code, currency_symbol, mobile_extension, language, inr_to_rate, percentage, display_order, allow_shipping, allow_billing, subject_to_vat, published } = req.body;
        let create_data = await commonQuery(Country, "create", { name, two_digit_code, three_digit_code, currency_code, currency_symbol, mobile_extension, language, inr_to_rate, percentage, display_order, allow_shipping, allow_billing, subject_to_vat, published });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editCountry: async (req, res) => {
        const { name, two_digit_code, three_digit_code, currency_code, currency_symbol, mobile_extension, language, inr_to_rate, percentage, display_order, allow_shipping, allow_billing, subject_to_vat, published, id } = req.body;
        let update_data = await commonQuery(Country, "findOneAndUpdate", { _id: id }, { name, two_digit_code, three_digit_code, currency_code, currency_symbol, mobile_extension, language, inr_to_rate, percentage, display_order, allow_shipping, allow_billing, subject_to_vat, published })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteCountry: async (req, res) => {
        let delete_data = await commonQuery(Country, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allStates: async (req, res) => {
        let datas_data = await commonQuery(State, "find", { country_id: req.body.id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneState: async (req, res) => {
        const get_data = await commonQuery(State, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createState: async (req, res) => {
        const { country_id, name, abbreviation, display_order, published } = req.body;
        let create_data = await commonQuery(State, "create", { name, abbreviation, display_order, published, country_id });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            let update_data = await commonQuery(Country, 'findOneAndUpdate', { _id: country_id }, { $push: { state_id: create_data.data._id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message })
            } else {
                res.status(200).json({ message: "Data created successfully." });
            }
        }
    },
    editState: async (req, res) => {
        const { name, abbreviation, display_order, published, id } = req.body;
        let update_data = await commonQuery(State, "findOneAndUpdate", { _id: id }, { name, abbreviation, display_order, published })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteState: async (req, res) => {
        let delete_data = await commonQuery(State, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            let update_data = await commonQuery(Country, "findOneAndUpdate", { _id: req.body.country_id }, { $pull: { state_id: req.body.id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message })
            } else {
                res.status(200).json({ message: "Data deleted successfully." })
            }
        }
    },

    allWeights: async (req, res) => {
        let datas_data = await commonQuery(Weight, "find", { country_id: req.body.id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneWeight: async (req, res) => {
        const get_data = await commonQuery(Weight, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createWeight: async (req, res) => {
        const { country_id, weight, rate } = req.body;
        let create_data = await commonQuery(Weight, "create", { weight, rate, country_id });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            let update_data = await commonQuery(Country, 'findOneAndUpdate', { _id: country_id }, { $push: { weight_id: create_data.data._id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message })
            } else {
                res.status(200).json({ message: "Data created successfully." });
            }
        }
    },
    editWeight: async (req, res) => {
        const { weight, rate, id } = req.body;
        let update_data = await commonQuery(Weight, "findOneAndUpdate", { _id: id }, { weight, rate })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteWeight: async (req, res) => {
        let delete_data = await commonQuery(Weight, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            let update_data = await commonQuery(Country, "findOneAndUpdate", { _id: req.body.country_id }, { $pull: { weight_id: req.body.id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message })
            } else {
                res.status(200).json({ message: "Data deleted successfully." })
            }
        }
    },

    allCities: async (req, res) => {
        let datas_data = await commonQuery(City, "find", { state_id: req.body.id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneCity: async (req, res) => {
        const get_data = await commonQuery(City, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createCity: async (req, res) => {
        const { state_id, name, display_order, } = req.body;
        let create_data = await commonQuery(City, "create", { name, display_order, state_id });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            let update_data = await commonQuery(State, 'findOneAndUpdate', { _id: state_id }, { $push: { city_id: create_data.data._id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message });
            } else {
                res.status(200).json({ message: "Data created successfully." });
            }
        }
    },
    editCity: async (req, res) => {
        const { name, display_order, id } = req.body;
        let update_data = await commonQuery(City, "findOneAndUpdate", { _id: id }, { name, display_order, })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteCity: async (req, res) => {
        let delete_data = await commonQuery(City, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            let update_data = await commonQuery(State, "findOneAndUpdate", { _id: req.body.state_id }, { $pull: { city_id: req.body.id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message })
            } else {
                res.status(200).json({ message: "Data deleted successfully." })
            }
        }
    },

    allPages: async (req, res) => {
        let datas_data = await commonQuery(Page, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    findOnePage: async (req, res) => {
        const { _id } = req.body;
        let datas_data = await commonQuery(Page, "findOne", { _id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createPage: async (req, res) => {
        const { name, title, short_dec, long_dec, page_url, display_order, search_engine_page_name, meta_title, meta_keyword, meta_dec, meta_tag, images, } = req.body;
        let create_data = await commonQuery(Page, "create", { name, title, short_dec, long_dec, page_url, display_order, search_engine_page_name, meta_title, meta_keyword, meta_dec, meta_tag, images, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editPage: async (req, res) => {
        const { name, title, short_dec, long_dec, page_url, display_order, search_engine_page_name, meta_title, meta_keyword, meta_dec, meta_tag, images, id } = req.body;
        let update_data = await commonQuery(Page, "findOneAndUpdate", { _id: id }, { name, title, short_dec, long_dec, page_url, display_order, search_engine_page_name, meta_title, meta_keyword, meta_dec, meta_tag, images, })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deletePage: async (req, res) => {
        let delete_data = await commonQuery(Page, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allSizeCharts: async (req, res) => {
        let datas_data = await commonQuery(SizeChart, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneSizeChart: async (req, res) => {
        const get_data = await commonQuery(SizeChart, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createSizeChart: async (req, res) => {
        const { name, slug, short_dec, long_dec, image, } = req.body;
        let create_data = await commonQuery(SizeChart, "create", { name, slug, short_dec, long_dec, image, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editSizeChart: async (req, res) => {
        const { name, slug, short_dec, long_dec, image, id } = req.body;
        let update_data = await commonQuery(SizeChart, "findOneAndUpdate", { _id: id }, { name, slug, short_dec, long_dec, image })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteSizeChart: async (req, res) => {
        let delete_data = await commonQuery(SizeChart, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allHowtoMeasures: async (req, res) => {
        let datas_data = await commonQuery(HowtoMeasure, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneHowtoMeasure: async (req, res) => {
        const get_data = await commonQuery(HowtoMeasure, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createHowtoMeasure: async (req, res) => {
        const { name, slug, short_dec, long_dec, image, } = req.body;
        let create_data = await commonQuery(HowtoMeasure, "create", { name, slug, short_dec, long_dec, image, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editHowtoMeasure: async (req, res) => {
        const { name, slug, short_dec, long_dec, image, id } = req.body;
        let update_data = await commonQuery(HowtoMeasure, "findOneAndUpdate", { _id: id }, { name, slug, short_dec, long_dec, image })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteHowtoMeasure: async (req, res) => {
        let delete_data = await commonQuery(HowtoMeasure, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allMeasurements: async (req, res) => {
        let datas_data = await commonQuery(Measurement, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneMeasurement: async (req, res) => {
        const get_data = await commonQuery(Measurement, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createMeasurement: async (req, res) => {
        const { name, slug, short_dec, available, image, } = req.body;
        let create_data = await commonQuery(Measurement, "create", { name, slug, short_dec, available, image, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editMeasurement: async (req, res) => {
        const { name, slug, short_dec, available, image, id } = req.body;
        let update_data = await commonQuery(Measurement, "findOneAndUpdate", { _id: id }, { name, slug, short_dec, available, image })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteMeasurement: async (req, res) => {
        let delete_data = await commonQuery(Measurement, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allMeasurementValues: async (req, res) => {
        let datas_data = await commonQuery(MeasurementValue, "find", { measurement_id: req.body.id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneMeasurementValue: async (req, res) => {
        const get_data = await commonQuery(MeasurementValue, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createMeasurementValue: async (req, res) => {
        const { measurement_id, name, title, attribute, short_dec, from, to, img_with_text, radio } = req.body
        let create_data = await commonQuery(MeasurementValue, "create", { measurement_id, name, title, attribute, short_dec, from, to, img_with_text, radio });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            let update_data = await commonQuery(Measurement, 'findOneAndUpdate', { _id: measurement_id }, { $push: { measurement_value_id: create_data.data._id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message });
            } else {
                res.status(200).json({ message: "Data created successfully." });
            }
        }
    },
    editMeasurementValue: async (req, res) => {
        const { name, title, attribute, short_dec, from, to, img_with_text, radio, id } = req.body;
        let update_data = await commonQuery(MeasurementValue, "findOneAndUpdate", { _id: id }, { name, title, attribute, short_dec, from, to, img_with_text, radio })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteMeasurementValue: async (req, res) => {
        let delete_data = await commonQuery(MeasurementValue, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allBlogCategorys: async (req, res) => {
        let datas_data = await commonQuery(BlogCategory, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },

    findOneBlogCategory: async (req, res) => {
        const { _id } = req.body;
        let datas_data = await commonQuery(BlogCategory, "findOne", { _id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },

    createBlogCategory: async (req, res) => {
        const { name, slug, parent_category_id, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, image, } = req.body;
        let create_data = await commonQuery(BlogCategory, "create", { name, slug, parent_category_id, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, image, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editBlogCategory: async (req, res) => {
        const { name, slug, parent_category_id, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, image, id } = req.body;
        let update_data = await commonQuery(BlogCategory, "findOneAndUpdate", { _id: id }, { name, slug, parent_category_id, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, image, })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data updated successfully." });
        }
    },
    deleteBlogCategory: async (req, res) => {
        let delete_data = await commonQuery(BlogCategory, "deleteOne", { _id: req.body.id });
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data deleted successfully." });
        }
    },

    allBlogPosts: async (req, res) => {
        const populateData = [{ path: "blog_category_id", select: "name" }]
        let datas_data = await commonQuery(BlogPost, "find", {}, {}, "", populateData);
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    findOneBlogPosts: async (req, res) => {
        const { _id } = req.body;
        const populateData = [{ path: "blog_category_id", select: "name" }]
        let datas_data = await commonQuery(BlogPost, "findOne", { _id }, {}, "", populateData);
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createBlogPost: async (req, res) => {
        const { title, url, blog_category_id, author, short_dec, long_dec, search_engine_page_name, meta_title, meta_dec, meta_keyword, meta_tag, allow_comment, published, image, } = req.body;
        let create_data = await commonQuery(BlogPost, "create", { title, url, blog_category_id, author, short_dec, long_dec, search_engine_page_name, meta_title, meta_dec, meta_keyword, meta_tag, allow_comment, published, image, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editBlogPost: async (req, res) => {
        const { title, url, blog_category_id, author, short_dec, long_dec, search_engine_page_name, meta_title, meta_dec, meta_keyword, meta_tag, allow_comment, published, image, id } = req.body;
        let update_data = await commonQuery(BlogPost, "findOneAndUpdate", { _id: id }, { title, url, blog_category_id, author, short_dec, long_dec, search_engine_page_name, meta_title, meta_dec, meta_keyword, meta_tag, allow_comment, published, image, })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteBlogPost: async (req, res) => {
        let delete_data = await commonQuery(BlogPost, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allEmailTemplates: async (req, res) => {
        let datas_data = await commonQuery(EmailTemplate, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneEmailTemplate: async (req, res) => {
        const get_data = await commonQuery(EmailTemplate, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createEmailTemplate: async (req, res) => {
        const { name, title, subject, from, reply, long_dec, is_active, image, } = req.body;
        let create_data = await commonQuery(EmailTemplate, "create", { name, title, subject, from, reply, long_dec, is_active, image, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editEmailTemplate: async (req, res) => {
        const { name, title, subject, from, reply, long_dec, is_active, image, id } = req.body;
        let update_data = await commonQuery(EmailTemplate, "findOneAndUpdate", { _id: id }, { name, title, subject, from, reply, long_dec, is_active, image, })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteEmailTemplate: async (req, res) => {
        let delete_data = await commonQuery(EmailTemplate, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allHelps: async (req, res) => {
        let datas_data = await commonQuery(Help, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneHelp: async (req, res) => {
        const get_data = await commonQuery(Help, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createHelp: async (req, res) => {
        const { name, display_order, short_dec, long_dec, category_id, meta_title, meta_dec, meta_tag, meta_keyword, published, image, } = req.body;
        let create_data = await commonQuery(Help, "create", { name, display_order, short_dec, long_dec, category_id, meta_title, meta_dec, meta_tag, meta_keyword, published, image, });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editHelp: async (req, res) => {
        const { name, display_order, short_dec, long_dec, category_id, meta_title, meta_dec, meta_tag, meta_keyword, published, image, id } = req.body;
        let update_data = await commonQuery(Help, "findOneAndUpdate", { _id: id }, { name, display_order, short_dec, long_dec, category_id, meta_title, meta_dec, meta_tag, meta_keyword, published, image, })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteHelp: async (req, res) => {
        let delete_data = await commonQuery(Help, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allFAQs: async (req, res) => {
        let datas_data = await commonQuery(FAQ, "find", { help_id: req.body.id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneFAQ: async (req, res) => {
        const get_data = await commonQuery(FAQ, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createFAQ: async (req, res) => {
        const { help_id, question, answer } = req.body;
        let create_data = await commonQuery(FAQ, "create", { question, answer, help_id });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            let update_data = await commonQuery(Help, 'findOneAndUpdate', { _id: help_id }, { $push: { faq_id: create_data.data._id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message })
            } else {
                res.status(200).json({ message: "Data created successfully." });
            }
        }
    },
    editFAQ: async (req, res) => {
        const { question, answer, published, id } = req.body;
        let update_data = await commonQuery(FAQ, "findOneAndUpdate", { _id: id }, { question, answer, published })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteFAQ: async (req, res) => {
        let delete_data = await commonQuery(FAQ, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            let update_data = await commonQuery(Country, "findOneAndUpdate", { _id: req.body.help_id }, { $pull: { faq_id: req.body.id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message })
            } else {
                res.status(200).json({ message: "Data deleted successfully." })
            }
        }
    },

    //Ravi Implement
    allStoreDetails: async (req, res) => {

        let datas_data = await commonQuery(StoreDetails, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createStoreDetail: async (req, res) => {
        const { store_name, store_url, store_email, store_short_description, store_long_description, meta_title, meta_keyword, meta_tag, meta_description, company_name, address_one, address_two, country_id, state_id, city_id, pincode, store_phone, whatsapp_number, bank_name, bank_account_name, bank_account_number, ifsc_code, gst_number, pan_number, notification_detail, full_logo, short_logo, favicon_logo, favicon_logo_image, banner_desktop, banner_mobile, desktop_cart_page, mobile_cart_page, desktop_customer_account_page, mobile_customer_account_page, desktop_blog_page, mobile_blog_page, desktop_help_page, mobile_help_page, social_facebook, social_twitter, social_instagram, social_pinterest, social_youtube, social_tumblr, social_linkedin, social_telegram, social_skype, social_snapchat, social_tiktok } = req.body

        let create_data = await commonQuery(StoreDetails, "create", { store_name, store_url, store_email, store_short_description, store_long_description, meta_title, meta_keyword, meta_tag, meta_description, company_name, address_one, address_two, country_id, state_id, city_id, pincode, store_phone, whatsapp_number, bank_name, bank_account_name, bank_account_number, ifsc_code, gst_number, pan_number, notification_detail, full_logo, short_logo, favicon_logo, favicon_logo_image, banner_desktop, banner_mobile, desktop_cart_page, mobile_cart_page, desktop_customer_account_page, mobile_customer_account_page, desktop_blog_page, mobile_blog_page, desktop_help_page, mobile_help_page, social_facebook, social_twitter, social_instagram, social_pinterest, social_youtube, social_tumblr, social_linkedin, social_telegram, social_skype, social_snapchat, social_tiktok });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editStoreDetail: async (req, res) => {
        const { store_name, store_url, store_email, store_short_description, store_long_description, meta_title, meta_keyword, meta_tag, meta_description, company_name, address_one, address_two, country_id, state_id, city_id, pincode, store_phone, whatsapp_number, bank_name, bank_account_name, bank_account_number, ifsc_code, gst_number, pan_number, notification_detail, full_logo, short_logo, favicon_logo, favicon_logo_image, banner_desktop, banner_mobile, desktop_cart_page, mobile_cart_page, desktop_customer_account_page, mobile_customer_account_page, desktop_blog_page, mobile_blog_page, desktop_help_page, mobile_help_page, social_facebook, social_twitter, social_instagram, social_pinterest, social_youtube, social_tumblr, social_linkedin, social_telegram, social_skype, social_snapchat, social_tiktok, id } = req.body;

        let update_data = await commonQuery(StoreDetails, "findOneAndUpdate", { _id: id }, { store_name, store_url, store_email, store_short_description, store_long_description, meta_title, meta_keyword, meta_tag, meta_description, company_name, address_one, address_two, country_id, state_id, city_id, pincode, store_phone, whatsapp_number, bank_name, bank_account_name, bank_account_number, ifsc_code, gst_number, pan_number, notification_detail, full_logo, short_logo, favicon_logo, favicon_logo_image, banner_desktop, banner_mobile, desktop_cart_page, mobile_cart_page, desktop_customer_account_page, mobile_customer_account_page, desktop_blog_page, mobile_blog_page, desktop_help_page, mobile_help_page, social_facebook, social_twitter, social_instagram, social_pinterest, social_youtube, social_tumblr, social_linkedin, social_telegram, social_skype, social_snapchat, social_tiktok })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteStoreDetail: async (req, res) => {
        let delete_data = await commonQuery(StoreDetails, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },
    allVendor: async (req, res) => {

        let datas_data = await commonQuery(Vendor, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createVendor: async (req, res) => {
        const { name, email, slug, description, meta_title, meta_keyword, meta_tag, meta_description, first_name, last_name, company, country_id, state_id, city_id, zip_number, phone_number, alternate_phone_number, whatsapp_number, bank_name, bank_account_name, bank_account_number, bank_ifsc_code, gst_number, bank_pan_number, vendor_note, active, image } = req.body

        let create_data = await commonQuery(Vendor, "create", { name, email, slug, description, meta_title, meta_keyword, meta_tag, meta_description, first_name, last_name, company, country_id, state_id, city_id, zip_number, phone_number, alternate_phone_number, whatsapp_number, bank_name, bank_account_name, bank_account_number, bank_ifsc_code, gst_number, bank_pan_number, vendor_note, active, image });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editVendor: async (req, res) => {
        const { name, email, slug, description, meta_title, meta_keyword, meta_tag, meta_description, first_name, last_name, company, country_id, state_id, city_id, zip_number, phone_number, alternate_phone_number, whatsapp_number, bank_name, bank_account_name, bank_account_number, bank_ifsc_code, gst_number, bank_pan_number, vendor_note, active, image, id } = req.body;

        let update_data = await commonQuery(Vendor, "findOneAndUpdate", { _id: id }, { name, email, slug, description, meta_title, meta_keyword, meta_tag, meta_description, first_name, last_name, company, country_id, state_id, city_id, zip_number, phone_number, alternate_phone_number, whatsapp_number, bank_name, bank_account_name, bank_account_number, bank_ifsc_code, gst_number, bank_pan_number, vendor_note, active, image })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteVendor: async (req, res) => {
        let delete_data = await commonQuery(Vendor, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },
    allParentCategory: async (req, res) => {

        let datas_data = await commonQuery(ParentCategory, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createParentCategory: async (req, res) => {
        const { name, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, category_image } = req.body

        let create_data = await commonQuery(ParentCategory, "create", { name, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, category_image });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editParentCategory: async (req, res) => {
        const { name, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, category_image, id } = req.body;

        let update_data = await commonQuery(ParentCategory, "findOneAndUpdate", { _id: id }, { name, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, category_image })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteParentCategory: async (req, res) => {
        let delete_data = await commonQuery(ParentCategory, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },
    allCategory: async (req, res) => {

        let datas_data = await commonQuery(Category, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createCategory: async (req, res) => {
        const { name, slug, parent_id, display_order, short_dec, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, featured_image, category_image } = req.body;

        let create_data = await commonQuery(Category, "create", { name, slug, parent_id, display_order, short_dec, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, featured_image, category_image });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editCategory: async (req, res) => {
        const { name, slug, parent_id, display_order, short_dec, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, featured_image, category_image, id } = req.body;

        let update_data = await commonQuery(Category, "findOneAndUpdate", { _id: id }, { name, slug, parent_id, display_order, short_dec, long_dec, meta_title, meta_keyword, meta_dec, meta_tag, published, featured, featured_image, category_image })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteCategory: async (req, res) => {
        let delete_data = await commonQuery(Category, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allProductTag: async (req, res) => {
        let datas_data = await commonQuery(ProductTag, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createProductTag: async (req, res) => {
        const { tag_name } = req.body
        let create_data = await commonQuery(ProductTag, "create", { tag_name });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editProductTag: async (req, res) => {
        const { tag_name, id } = req.body;
        let update_data = await commonQuery(ProductTag, "findOneAndUpdate", { _id: id }, { tag_name })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteProductTag: async (req, res) => {
        let delete_data = await commonQuery(ProductTag, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allProductAttribute: async (req, res) => {
        let datas_data = await commonQuery(ProductAttribute, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneProductAttribute: async (req, res) => {
        const get_data = await commonQuery(ProductAttribute, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createProductAttribute: async (req, res) => {
        const { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, combine_value, image } = req.body
        let create_data = await commonQuery(ProductAttribute, "create", { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, combine_value, image });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editProductAttribute: async (req, res) => {
        const { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, combine_value, image, id } = req.body;

        let update_data = await commonQuery(ProductAttribute, "findOneAndUpdate", { _id: id }, { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, published, combine_value, image })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteProductAttribute: async (req, res) => {
        let delete_data = await commonQuery(ProductAttribute, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allAttributeValue: async (req, res) => {
        let datas_data = await commonQuery(AttributeValue, "find", { product_attribute_id: req.body.id });
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    oneAttributeValue: async (req, res) => {
        const get_data = await commonQuery(AttributeValue, "findOne", { _id: req.body.id });
        if (get_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            res.status(200).json(get_data.data)
        }
    },
    createAttributeValue: async (req, res) => {
        const { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, price, quantity, ship_day, product_attribute_id, published, image } = req.body
        let create_data = await commonQuery(AttributeValue, "create", { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, price, quantity, ship_day, product_attribute_id, published, image });
        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            let update_data = await commonQuery(ProductAttribute, 'findOneAndUpdate', { _id: product_attribute_id }, { $push: { attribute_value_id: create_data.data._id } });
            if (update_data.status != 1) {
                res.status(400).json({ message: common_error_message });
            } else {
                res.status(200).json({ message: "Data created successfully." });
            }
        }
    },
    editAttributeValue: async (req, res) => {
        const { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, price, quantity, ship_day, published, image, id } = req.body;
        let update_data = await commonQuery(AttributeValue, "findOneAndUpdate", { _id: id }, { name, slug, display_order, short_dec, long_dec, meta_title, meta_dec, meta_keyword, meta_tag, price, quantity, ship_day, published, image })
        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteAttributeValue: async (req, res) => {
        let delete_data = await commonQuery(AttributeValue, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allProductCombination: async (req, res) => {
        let datas_data = await commonQuery(ProductCombination, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createProductCombination: async (req, res) => {
        const { attributes_name, attributes_url, product_attribute_id, short_description, long_description, meta_title, meta_keyword, meta_description, meta_tag } = req.body

        let create_data = await commonQuery(ProductCombination, "create", { attributes_name, attributes_url, product_attribute_id, short_description, long_description, meta_title, meta_keyword, meta_description, meta_tag });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editProductCombination: async (req, res) => {
        const { attributes_name, attributes_url, product_attribute_id, short_description, long_description, meta_title, meta_keyword, meta_description, meta_tag, id } = req.body;

        let update_data = await commonQuery(ProductCombination, "findOneAndUpdate", { _id: id }, { attributes_name, attributes_url, product_attribute_id, short_description, long_description, meta_title, meta_keyword, meta_description, meta_tag })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteProductCombination: async (req, res) => {
        let delete_data = await commonQuery(ProductCombination, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },

    allProduct: async (req, res) => {

        let datas_data = await commonQuery(Product, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createProduct: async (req, res) => {
        const { product_name, short_description, point_one, point_two, point_three, point_four, point_five, sku, hsn_code, commodity_type, category_id, vendor_id, ship_day_id, size_chart_id, how_to_measure_id, product_tag_id, inventory, published, is_featured, sale, allow_customer_review, is_available, mark_as_new, is_gift_card, is_rental, search_engine_friendly_page_name, meta_title, meta_keyword, meta_description, meta_tag, purchase_price, actual_price, sales_price, tax_percentage, discount_id, disable_buy_button, disable_wishlist_button, available_for_pre_order, call_for_price, shipping_weight, shipping_enable, free_shipping, additional_shipping_charges_flag, additional_shipping_charges, product_images, product_attribute_id, measurement_name, measurement_value } = req.body

        let create_data = await commonQuery(Product, "create", { product_name, short_description, point_one, point_two, point_three, point_four, point_five, sku, hsn_code, commodity_type, category_id, vendor_id, ship_day_id, size_chart_id, how_to_measure_id, product_tag_id, inventory, published, is_featured, sale, allow_customer_review, is_available, mark_as_new, is_gift_card, is_rental, search_engine_friendly_page_name, meta_title, meta_keyword, meta_description, meta_tag, purchase_price, actual_price, sales_price, tax_percentage, discount_id, disable_buy_button, disable_wishlist_button, available_for_pre_order, call_for_price, shipping_weight, shipping_enable, free_shipping, additional_shipping_charges_flag, additional_shipping_charges, product_images, product_attribute_id, measurement_name, measurement_value });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editProduct: async (req, res) => {
        const { product_name, short_description, point_one, point_two, point_three, point_four, point_five, sku, hsn_code, commodity_type, category_id, vendor_id, ship_day_id, size_chart_id, how_to_measure_id, product_tag_id, inventory, published, is_featured, sale, allow_customer_review, is_available, mark_as_new, is_gift_card, is_rental, search_engine_friendly_page_name, meta_title, meta_keyword, meta_description, meta_tag, purchase_price, actual_price, sales_price, tax_percentage, discount_id, disable_buy_button, disable_wishlist_button, available_for_pre_order, call_for_price, shipping_weight, shipping_enable, free_shipping, additional_shipping_charges_flag, additional_shipping_charges, product_images, product_attribute_id, measurement_name, measurement_value, id } = req.body;

        let update_data = await commonQuery(Product, "findOneAndUpdate", { _id: id }, { product_name, short_description, point_one, point_two, point_three, point_four, point_five, sku, hsn_code, commodity_type, category_id, vendor_id, ship_day_id, size_chart_id, how_to_measure_id, product_tag_id, inventory, published, is_featured, sale, allow_customer_review, is_available, mark_as_new, is_gift_card, is_rental, search_engine_friendly_page_name, meta_title, meta_keyword, meta_description, meta_tag, purchase_price, actual_price, sales_price, tax_percentage, discount_id, disable_buy_button, disable_wishlist_button, available_for_pre_order, call_for_price, shipping_weight, shipping_enable, free_shipping, additional_shipping_charges_flag, additional_shipping_charges, product_images, product_attribute_id, measurement_name, measurement_value })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteProduct: async (req, res) => {
        let delete_data = await commonQuery(Product, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },
    allManualPage: async (req, res) => {

        let datas_data = await commonQuery(ManualPage, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createManualPage: async (req, res) => {
        const { name, short_description, long_description, parent_category_id, meta_title, meta_description, meta_keyword, meta_tag, product_id, published, image } = req.body

        let create_data = await commonQuery(ManualPage, "create", { name, short_description, long_description, parent_category_id, meta_title, meta_description, meta_keyword, meta_tag, product_id, published, image });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editManualPage: async (req, res) => {
        const { name, short_description, long_description, parent_category_id, meta_title, meta_description, meta_keyword, meta_tag, product_id, published, image, id } = req.body;

        let update_data = await commonQuery(ManualPage, "findOneAndUpdate", { _id: id }, { name, short_description, long_description, parent_category_id, meta_title, meta_description, meta_keyword, meta_tag, product_id, published, image })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteManualPage: async (req, res) => {
        let delete_data = await commonQuery(ManualPage, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },
    allProductGiftCard: async (req, res) => {

        let datas_data = await commonQuery(ProductGiftCard, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createProductGiftCard: async (req, res) => {
        const { initial_value, initial_percentage, coupon_code, customer_name, sender_name, sender_email, message, gift_card_active } = req.body

        let create_data = await commonQuery(ProductGiftCard, "create", { initial_value, initial_percentage, coupon_code, customer_name, sender_name, sender_email, message, gift_card_active });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editProductGiftCard: async (req, res) => {
        const { initial_value, initial_percentage, coupon_code, customer_name, sender_name, sender_email, message, gift_card_active, id } = req.body;

        let update_data = await commonQuery(ProductGiftCard, "findOneAndUpdate", { _id: id }, { initial_value, initial_percentage, coupon_code, customer_name, sender_name, sender_email, message, gift_card_active })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteProductGiftCard: async (req, res) => {
        let delete_data = await commonQuery(ProductGiftCard, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    },
    allOrder: async (req, res) => {

        let datas_data = await commonQuery(Order, "find", {});
        if (datas_data.status != 1) {
            res.status(400).json({ message: "Data not found." })
        } else {
            let data = datas_data.data;
            res.status(200).json(data)
        }
    },
    createOrder: async (req, res) => {
        const { order_status, order_number, order_date, customer_email, customer_ip, address_type, shipping_method, payment_status, payment_method, transaction_id, order_subtotal, order_shipping, gift_card, order_total, order_note, product_id } = req.body

        let create_data = await commonQuery(Order, "create", { order_status, order_number, order_date, customer_email, customer_ip, address_type, shipping_method, payment_status, payment_method, transaction_id, order_subtotal, order_shipping, gift_card, order_total, order_note, product_id });

        if (create_data.status != 1) {
            res.status(400).json({ message: common_error_message });
        } else {
            res.status(200).json({ message: "Data created successfully." });
        }
    },
    editOrder: async (req, res) => {
        const { order_status, order_number, order_date, customer_email, customer_ip, address_type, shipping_method, payment_status, payment_method, transaction_id, order_subtotal, order_shipping, gift_card, order_total, order_note, product_id, id } = req.body;

        let update_data = await commonQuery(Order, "findOneAndUpdate", { _id: id }, { order_status, order_number, order_date, customer_email, customer_ip, address_type, shipping_method, payment_status, payment_method, transaction_id, order_subtotal, order_shipping, gift_card, order_total, order_note, product_id })

        if (update_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data updated successfully." })
        }
    },
    deleteOrder: async (req, res) => {
        let delete_data = await commonQuery(Order, "deleteOne", { _id: req.body.id })
        if (delete_data.status != 1) {
            res.status(400).json({ message: common_error_message })
        } else {
            res.status(200).json({ message: "Data deleted successfully." })
        }
    }

}