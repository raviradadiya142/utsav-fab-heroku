
// import files
const { commonQuery, common_error_message } = require('../../helper/common_helper');

// import modules
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// import models file
const Admin = require('../../models/admin');

module.exports = {
    adminSignin: async (req, res) => {
        const { username, password, security_code } = req.body;
        let check_data = await commonQuery(Admin, "findOne", { username, security_code });
        if (check_data.status != 1) {
            return res.status(400).json({ message: "Invalid Username." });
        }
        else {
            let check_pass = bcrypt.compareSync(password, check_data.data.password);
            if (!check_pass) {
                return res.status(400).json({ message: "Invalid Password." });
            }
            let jwt_token = jwt.sign({ _id: check_data.data._id }, 'process.env.ACCESS_TOKEN_SECRET_KEY', { expiresIn: '1d' });
            let token = Buffer.from(jwt_token).toString();
            return res.status(200).json({ message: "Welcome to admin panel.", token });
        }
    },
}


// const fnc = async () => {
//     const hash = bcrypt.hashSync('123456', 9);
//     await Admin.create({ username: "admin", password: hash, security_code: 'utsavfab' })
// }
// fnc()