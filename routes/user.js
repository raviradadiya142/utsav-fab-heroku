const router = require('express').Router();

const { signup, signin, } = require('./user/auth');
const dashboard = require('./user/dashboard');

const { userSignupValidator, userSigninValidator } = require('../validation/user');
const { runValidation } = require('../validation/index');


// user auth
router.post('/signup', userSignupValidator, runValidation, signup);
router.post('/signin', userSigninValidator, runValidation, signin);

// dashboard
router.post('/dashboard', dashboard.userDashboard)


module.exports = router;
