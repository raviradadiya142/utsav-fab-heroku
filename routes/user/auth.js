// import files
const helper = require('../../helper/common_helper');

// import modules
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// import models file
const Customer = require('../../models/customer');

module.exports = {

    signup: async function (req, res) {
        const { email, password, first_name, last_name, compony_name, gender, address_type, customer_role, } = req.body;

        let check_data = await commonQuery(Customer, "findOne", { email });
        if (check_data.status != 1) {
            let hash = await bcrypt.hashSync(password, 9);
            let create_res = await commonQuery(Customer, "create", { email, password: hash });
            if (create_res.status != 1) {
                return res.status(config.BAD_REQUEST).json({ message: common_error_message });
            } else {
                return res.status(config.OK_STATUS).json({ message: "Customer created successfully.", customer_id: create_res.data._id });
            }
        }
        else {
            return res.status(config.BAD_REQUEST).json({ message: "Email already exist." });
        }
    },

    signin: async function (req, res) {
        const { email, password } = req.body;
        let check_data = await commonQuery(Customer, "findOne", { email });
        if (check_data.status != 1) {
            return res.status(config.BAD_REQUEST).json({ message: "invalid email." });
        }
        else {
            let check_pass = await bcrypt.compare(password, check_data.data.password);
            if (!check_pass) {
                return res.status(config.BAD_REQUEST).json({ message: "email and password not match." });
            }
            return res.status(config.OK_STATUS).json({ message: "Welcome to user panel.", user_id: check_data.data._id });
        }
    },

    // userForgotPassword: async function (req, res) {
    //     const { email } = req.body;
    //     const protocol = req.protocol
    //     const host = req.get('host')

    //     const condition = { email: email }
    //     let check_data = await commonQuery(Customer, "findOne", condition);

    //     if (check_data.status != 1) {
    //         return res.status(config.BAD_REQUEST).json({ message: common_error_message });
    //     }
    //     else {
    //         const token = jwt.sign({ _id: check_data.data._id }, config.REFRESH_TOKEN_SECRET_KEY, { expiresIn: '10m' })
    //         const encoded_token = Buffer.from(token).toString('base64');
    //         const update_res = await commonQuery(Customer, "findOneAndUpdate", condition, { flag: 0 });
    //         let mail_res = await mail_common_mail(check_data.data.email, "Reset Password", "common_html", `${protocol}://${host}/reset-password/${encoded_token}`)
    //         await mail_res.transporter.sendMail(mail_res.emailData, (err, info) => {
    //             if (err || !info) {
    //                 return res.status(config.BAD_REQUEST).json({ "message": "Error occured while sending mail." });
    //             } else {
    //                 return res.status(config.OK_STATUS).json({ "message": "Reset link was sent to your email address." });
    //             }
    //         });
    //     }
    // },

    // userResetPassword: async function (req, res) {
    //     const { password, confirm_password, token } = req.body;
    //     jwt.verify(Buffer.from(token).toString(), config.REFRESH_TOKEN_SECRET_KEY, async function (err, decoded) {
    //         if (err || !decoded) {
    //             req.flash('successOrError', "Link has been expired.")
    //             return res.redirect(`reset-password/${req.body.token}`);
    //             // return res.status(config.BAD_REQUEST).json({ message: "Link has been expired." })
    //         } else {
    //             if (password != confirm_password) {
    //                 req.flash('successOrError', "Password not match.")
    //                 return res.redirect(`reset-password/${req.body.token}`);
    //             }
    //             const condition = { _id: decoded._id, flag: 0 }
    //             const hash_password = bcrypt.hashSync(password, 10);
    //             const update_data = await commonQuery(Customer, "findOneAndUpdate", condition, { password: hash_password, flag: 1 });
    //             req.flash('successOrError', "Password has been changed.")
    //             return res.redirect(`/`);
    //         }
    //     });
    // },

    // userProfileUpdate: async function (req, res) {
    //     const { user_id, name, email, old_password, new_password } = req.body;

    //     const condition = { _id: user_id }
    //     let check_data = await commonQuery(Customer, "findOne", condition);
    //     if (check_data.status != 1) {
    //         return res.status(config.BAD_REQUEST).json({ message: "Invalid Customer." })
    //     } else {
    //         let check_email_data = await commonQuery(Customer, "findOne", { email: email })
    //         let newName = (name == undefined || name == '') ? check_data.data.name : name;
    //         let newEmail = (email == undefined || email == '') ? check_data.data.email : email;
    //         if (old_password != undefined) {
    //             let hash = await bcrypt.compare(old_password, check_data.data.password)
    //             if (!hash) {
    //                 return res.status(config.BAD_REQUEST).json({ message: "Old password is incorrect." })
    //             }
    //         }
    //         let newPassword = (new_password == undefined || new_password == '') ? check_data.data.password : bcrypt.hashSync(new_password, 10);
    //         if (check_email_data.status == 1) {
    //             return res.status(config.BAD_REQUEST).json({ message: "Email already exist." })
    //         }
    //         const update = { name: newName, email: newEmail, password: newPassword }
    //         let update_data = await commonQuery(Customer, "findOneAndUpdate", condition, update);
    //         if (update_data.status != 1) {
    //             return res.status(config.BAD_REQUEST).json({ message: common_error_message });
    //         } else {
    //             return res.status(config.OK_STATUS).json({ message: "Customer updated successfully." })
    //         }
    //     }
    // },

}

