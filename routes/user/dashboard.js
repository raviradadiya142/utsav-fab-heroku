// import files
const { commonQuery, common_error_message } = require('../../helper/common_helper');

// import modules

// import models file
const Customer = require('../../models/customer');

module.exports = {

    userDashboard: async function (req, res) {
        const { user_id } = req.body;
        let check_data = await commonQuery(Customer, "findOne", { _id: user_id });
        if (check_data.status != 1) {
            return res.status(config.BAD_REQUEST).json({ message: "invalid user." });
        }
        else {
            return res.status(config.OK_STATUS).json({ data: check_data.data })
        }
    },

}
