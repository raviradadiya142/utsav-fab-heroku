const router = require('express').Router();

const { adminSignin } = require('./admin/auth');
const { allCountries, createCountry, editCountry, deleteCountry, allStates, createState, editState, deleteState, allCities, createCity, editCity, deleteCity, allPages, editPage, deletePage, createPage, allSizeCharts, createSizeChart, editSizeChart, deleteSizeChart, allHowtoMeasures, allMeasurements, oneMeasurement, createMeasurement, editMeasurement, deleteMeasurement, allMeasurementValues, oneMeasurementValue, createMeasurementValue, editMeasurementValue, deleteMeasurementValue, allBlogCategorys, findOneBlogCategory, createBlogCategory, editBlogCategory, deleteBlogCategory, allBlogPosts, findOneBlogPosts, createBlogPost, editBlogPost, deleteBlogPost, allWeights, createWeight, editWeight, deleteWeight, allEmailTemplates, createEmailTemplate, editEmailTemplate, deleteEmailTemplate, allHelps, createHelp, editHelp, deleteHelp, allFAQs, createFAQ, editFAQ, deleteFAQ, allStoreDetails, createStoreDetail, editStoreDetail, deleteStoreDetail, allVendor, createVendor, editVendor, deleteVendor, allParentCategory, createParentCategory, editParentCategory, deleteParentCategory, allCategory, createCategory, editCategory, deleteCategory, allProductTag, createProductTag, editProductTag, deleteProductTag, allProductAttribute, createProductAttribute, editProductAttribute, deleteProductAttribute, allProductCombination, createProductCombination, editProductCombination, deleteProductCombination, allProduct, createProduct, editProduct, deleteProduct, allManualPage, createManualPage, editManualPage, deleteManualPage, allProductGiftCard, createProductGiftCard, editProductGiftCard, deleteProductGiftCard, allOrder, createOrder, editOrder, deleteOrder, oneCountry, oneState, oneCity, oneWeight, findOnePage, oneSizeChart, createHowtoMeasure, editHowtoMeasure, deleteHowtoMeasure, oneHowtoMeasure, oneFAQ, oneHelp, oneEmailTemplate, oneProductAttribute, allAttributeValue, oneAttributeValue, createAttributeValue, editAttributeValue, deleteAttributeValue, } = require('./admin/dashboard');

const { signinValidator } = require('../validation/admin');
const { runValidation } = require('../validation/index');

// Admin auth
router.post('/signin', signinValidator, runValidation, adminSignin);

router.post('/all_countries', allCountries);
router.post('/country', oneCountry)
router.post('/create_country', createCountry);
router.post('/edit_country', editCountry);
router.post('/delete_country', deleteCountry);

router.post('/all_states', allStates);
router.post('/state', oneState)
router.post('/create_state', createState);
router.post('/edit_state', editState);
router.post('/delete_state', deleteState);

router.post('/all_weights', allWeights);
router.post('/weight', oneWeight);
router.post('/create_weight', createWeight);
router.post('/edit_weight', editWeight);
router.post('/delete_weight', deleteWeight);

router.post('/all_cities', allCities);
router.post('/city', oneCity);
router.post('/create_city', createCity);
router.post('/edit_city', editCity);
router.post('/delete_city', deleteCity);

router.post('/all_pages', allPages);
router.post('/find_one_page', findOnePage);
router.post('/create_page', createPage);
router.post('/edit_page', editPage);
router.post('/delete_page', deletePage);

router.post('/all_sizecharts', allSizeCharts);
router.post('/sizechart', oneSizeChart);
router.post('/create_sizechart', createSizeChart);
router.post('/edit_sizechart', editSizeChart);
router.post('/delete_sizechart', deleteSizeChart);

router.post('/all_howtoMeasure', allHowtoMeasures);
router.post('/howtoMeasure', oneHowtoMeasure);
router.post('/create_howToMeasure', createHowtoMeasure);
router.post('/edit_howtoMeasure', editHowtoMeasure);
router.post('/delete_howtoMeasure', deleteHowtoMeasure)

router.post('/all_measurements', allMeasurements);
router.post('/one_measurement', oneMeasurement)
router.post('/create_measurement', createMeasurement);
router.post('/edit_measurement', editMeasurement);
router.post('/delete_measurement', deleteMeasurement);

router.post('/all_measurement_vallues', allMeasurementValues);
router.post('/one_measurement_vallue', oneMeasurementValue)
router.post('/create_measurement_vallue', createMeasurementValue);
router.post('/edit_measurement_vallue', editMeasurementValue);
router.post('/delete_measurement_vallue', deleteMeasurementValue);

router.post('/all_blogcategorys', allBlogCategorys);
router.post('/find_one_blogcategory', findOneBlogCategory);
router.post('/create_blogcategory', createBlogCategory);
router.post('/edit_blogcategory', editBlogCategory);
router.post('/delete_blogcategory', deleteBlogCategory);

router.post('/all_blogposts', allBlogPosts);
router.post('/find_one_blogposts', findOneBlogPosts);
router.post('/create_blogpost', createBlogPost);
router.post('/edit_blogpost', editBlogPost);
router.post('/delete_blogpost', deleteBlogPost);

router.post('/all_email_templates', allEmailTemplates);
router.post('/one_email_template', oneEmailTemplate);
router.post('/create_email_template', createEmailTemplate);
router.post('/edit_email_template', editEmailTemplate);
router.post('/delete_email_template', deleteEmailTemplate);

router.post('/all_helps', allHelps);
router.post('/one_help', oneHelp);
router.post('/create_help', createHelp);
router.post('/edit_help', editHelp);
router.post('/delete_help', deleteHelp);

router.post('/all_faqs', allFAQs);
router.post('/one_faq', oneFAQ);
router.post('/create_faq', createFAQ);
router.post('/edit_faq', editFAQ);
router.post('/delete_faq', deleteFAQ);

//Ravi Implement

router.post('/all_store_detail', allStoreDetails);
router.post('/create_store_detail', createStoreDetail);
router.post('/edit_store_detail', editStoreDetail);
router.post('/delete_store_detail', deleteStoreDetail);

router.post('/all_vendor', allVendor);
router.post('/create_vendor', createVendor);
router.post('/edit_vendor', editVendor);
router.post('/delete_vendor', deleteVendor);

router.post('/all_parent_category', allParentCategory);
router.post('/create_parent_category', createParentCategory);
router.post('/edit_parent_category', editParentCategory);
router.post('/delete_parent_category', deleteParentCategory);

router.post('/all_category', allCategory);
router.post('/create_category', createCategory);
router.post('/edit_category', editCategory);
router.post('/delete_category', deleteCategory);

router.post('/all_product_tag', allProductTag);
router.post('/create_product_tag', createProductTag);
router.post('/edit_product_tag', editProductTag);
router.post('/delete_product_tag', deleteProductTag);

router.post('/all_product_attribute', allProductAttribute);
router.post('/one_product_attribute', oneProductAttribute);
router.post('/create_product_attribute', createProductAttribute);
router.post('/edit_product_attribute', editProductAttribute);
router.post('/delete_product_attribute', deleteProductAttribute);

router.post('/all_attribute_value', allAttributeValue);
router.post('/one_attribute_value', oneAttributeValue);
router.post('/create_attribute_value', createAttributeValue);
router.post('/edit_attribute_value', editAttributeValue);
router.post('/delete_attribute_value', deleteAttributeValue);

router.post('/all_product_combination', allProductCombination);
router.post('/create_product_combination', createProductCombination);
router.post('/edit_product_combination', editProductCombination);
router.post('/delete_product_combination', deleteProductCombination);

router.post('/all_product', allProduct);
router.post('/create_product', createProduct);
router.post('/edit_product', editProduct);
router.post('/delete_product', deleteProduct);

router.post('/all_manual_page', allManualPage);
router.post('/create_manual_page', createManualPage);
router.post('/edit_manual_page', editManualPage);
router.post('/delete_manual_page', deleteManualPage);

router.post('/all_product_gift_card', allProductGiftCard);
router.post('/create_product_gift_card', createProductGiftCard);
router.post('/edit_product_gift_card', editProductGiftCard);
router.post('/delete_product_gift_card', deleteProductGiftCard);

router.post('/all_order', allOrder);
router.post('/create_order', createOrder);
router.post('/edit_order', editOrder);
router.post('/delete_order', deleteOrder);

module.exports = router;