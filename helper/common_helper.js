// import files and modules

module.exports = {
    common_error_message: "Error, please try again.!",

    commonQuery: async function (model, query, data, update = {}, select = "", populate = null) {
        try {
            let res;
            switch (query) {
                case "find":
                    res = await model.find(data).select(select).populate(populate);
                    break;
                case "findOne":
                    res = await model.findOne(data).select(select).populate(populate);
                    break;
                case "create":
                    res = await model.create(data);
                    break;
                case "findOneAndUpdate":
                    res = await model.findOneAndUpdate(data, update, { new: true });
                    break;
                case "upsert":
                    res = await model.findOneAndUpdate(data, update, { upsert: true, new: true });
                    break;
                case "deleteOne":
                    res = await model.deleteOne(data);
                    break;
                case "deleteMany":
                    res = await model.deleteMany(data);
                    break;
            }
            if (!res || res.length == 0 || !data) {
                return {
                    status: 2,
                    message: "Error, please try again."
                }
            } else {
                return { status: 1, data: res }
            }
        } catch (error) {
            return { status: 0, error: error }
        }
    },
    
}