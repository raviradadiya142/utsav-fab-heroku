const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

//Set up default mongoose connection
// mongoose.connect(mongoDB, { auth: { authdb: "admin" }, useMongoClient: true });
mongoose.connect('mongodb+srv://utsavfab:5nQSRtGvP03i1Zvf@utsavdb.qggwt.mongodb.net/utsav_fab?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
});

//Get the default connection
let db = mongoose.connection;

// CONNECTION EVENTS
// When successfully connected
db.on('connected', function () {
    console.log('Mongoose default connection connected.')
});

// If the connection throws an error
db.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
db.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function () {
    db.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});